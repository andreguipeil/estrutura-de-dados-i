#include <stdio.h>
#include <stdlib.h>
#include "fila.h"


// aloca, inicializa e retorna um ponteiro para um descritor de ﬁla (ou NULL);
desc_fila *init(){
  desc_fila *list;
	
	if (!(list = malloc(sizeof(desc_fila)))){
		printf("\n");
		printf("erro.");
		printf("\n");
		return 0;	
	}
	
	list->head = NULL;
	list->tail = NULL;
	list->tamanho = 0;
	
return list;
}

// insere um novo elemento na ﬁla, retornando 0 em caso de erro, 1 se OK.
int enqueue(struct elemento * x, desc_fila * descritor){
	struct nodo *aux;
	
	aux = malloc(sizeof(struct nodo));
	aux->ptr = x;
	aux->next = NULL;
	
 // se for a primeira insercao
	if (descritor->tamanho == 0){
		descritor->head = aux;
		descritor->tail = aux;
		descritor->tamanho++;
		return 1;
	} else { // se tiver elemento na lista	
		descritor->tail->next = aux;
		descritor->tail = aux;
		descritor->tamanho++;
		return 1;
	}
	
	
	
}

// retira um elemento da ﬁla, colocando-o no elemento x, retornando 0 em caso de erro e 1 se OK;
int dequeue(struct elemento * x, desc_fila * descritor){
	struct nodo *aux;
	
		if (descritor->head == NULL){
			return 0;
		}	
		
			if ((descritor->head == descritor->tail)){
				x = descritor->head->ptr;
					
					printf("\n");
					printf("Elemento retirado da Fila: %d",x->chave);
					printf("\n");
					
				descritor->head = NULL;
				descritor->tail = NULL;
				descritor->tamanho--; 
				return 1;
			} else {
				x = descritor->head->ptr;		
				
					printf("\n");
					printf("Elemento retirado da Fila: %d",x->chave);
					printf("\n");
				
				aux = descritor->head->next;
				descritor->head = NULL;
				descritor->head = aux;
				descritor->tamanho--;		
				return 1;
			}

}
// retorna o comprimento da ﬁla;
unsigned int length(desc_fila * descritor){
	unsigned int p;

		p = descritor->tamanho;
	
return p;
}

// imprime conteúdo do nodo apontado por ptr e retorna 0 em caso de erro ou 1.
int print(struct nodo * temp){
	
	printf("NODO\n");
	printf("---\n");
	printf("\n");
	printf(" [%d] %p", temp->ptr->chave, temp->next);
	printf("\n");
	
return 1;
}


