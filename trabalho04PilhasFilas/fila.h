#ifndef FILA_H
#define FILA_H

struct elemento {
	int chave;
};

struct nodo {
	struct elemento *ptr;
	struct nodo *next;
};

typedef struct { 
	
	struct nodo *head, *tail;
	unsigned int tamanho;
	
} desc_fila;

//mainfilas

int main();
int menu(desc_fila *descritor);
void inserir(desc_fila *descritor);
int printar(desc_fila *descritor);
void retira(desc_fila *descritor);
void tamanho(desc_fila *descritor);

// filas.c

// aloca, inicializa e retorna um ponteiro para um descritor de ﬁla (ou NULL);
desc_fila *init();

// insere um novo elemento na ﬁla, retornando 0 em caso de erro, 1 se OK.
int enqueue(struct elemento * x, desc_fila * descritor);

// retira um elemento da ﬁla, colocando-o no elemento x, retornando 0 em caso de erro e 1 se OK;
int dequeue(struct elemento * x, desc_fila * descritor);

// retorna o comprimento da ﬁla;
unsigned int length(desc_fila * descritor);

// imprime conteúdo do nodo apontado por ptr e retorna 0 em caso de erro ou 1.
int print(struct nodo * ptr);


#endif
