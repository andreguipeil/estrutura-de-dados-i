#include <stdio.h>
#include <stdlib.h>
#include "pilha.h"


desc_pilha *init(){
  desc_pilha *list;
	
	if (!(list = malloc(sizeof(desc_pilha)))){
		printf("\n");
		printf("Erro no malloc.");
		printf("\n");
		return 0;	
	}
	
	list->head = NULL;
	list->tail = NULL;
	list->tamanho = 0;
	
return list;
}


int push(struct elemento * x, desc_pilha * descritor){
	struct nodo *new;
	
	if (!(new = malloc(sizeof(struct nodo)))){
		printf("\n");
		printf("Erro.");
		printf("\n");
		return 0;	
	}
 
	new->ptr = x;
	new->prev = NULL;
 
	if (descritor->tamanho == 0){
		descritor->head = new;
		descritor->tail = new;
		descritor->tamanho++;
		return 1;
	} else {
		new->prev = descritor->tail;
		descritor->tail = new;	
		descritor->tamanho++;
		return 1;
	}
	//free(new);
}


int pop(struct elemento * x, desc_pilha * descritor){
struct nodo *aux;

	if (descritor->head == NULL) {
		return 0;
	}
	
	if (descritor->tail == descritor->head){
				
		x = descritor->tail->ptr;
			
			printf("\n");
			printf("Elemento desempilhado: %d", x->chave);
			printf("\n");
		descritor->head = NULL;	
		descritor->tail = NULL;
		descritor->tamanho--;

	return 1;
	} else {
	
		x = descritor->tail->ptr;
			
			printf("\n");
			printf("Elemento desempilhado: %d", x->chave);
			printf("\n");
		
		aux = descritor->tail->prev;
		descritor->tail = NULL;	
		descritor->tail = aux;
		descritor->tamanho--;
	return 1;
	}
	

}

unsigned int length(desc_pilha * descritor){
unsigned int p;	

	p = descritor->tamanho;
	
return p;
}

// imprime conteúdo do nodo apontado por ptr e retorna 0 em caso de erro ou 1.
int print(struct nodo * temp){
	
	printf("NODO\n");
	printf("---\n");
	printf("\n");
	printf("%p [%d]", temp->prev, temp->ptr->chave);
	printf("\n");
	
return 1;
}
