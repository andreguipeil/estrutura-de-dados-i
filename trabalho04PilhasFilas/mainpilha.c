#include <stdio.h>
#include <stdlib.h>
#include "pilha.h"

// Andre Guimaraes Peil - ED1

// ------------------------------------------------------------------------------
// Funcao main
// ------------------------------------------------------------------------------

int main() {

desc_pilha * descritor;
int escolha;
descritor = init();
	
	
	for ( ; ; ){
		escolha = menu(descritor);
		switch(escolha){
			
			case 1: emp(descritor);
			break; 
			case 2: dezemp(descritor);
			break; 
			case 3: tamanho(descritor);
			break;
			case 4: printar(descritor);
			break;
			case 5: exit(0);
			break;
		}
	}
}

// ------------------------------------------------------------------------------
// Funcao menu
// ------------------------------------------------------------------------------

int menu(desc_pilha * descritor) {
	int c=0;
		
		printf("PRESSIONE ENTER PARA CONTINUAR...");
		getchar();
		getchar();
		system("clear");
		
		printf("\n");
		printf("--- ");
		printf("\n");
		printf("Lista com o metodo FILA");
		printf("\n");
		
		do {
			printf("-- MENU:\n");
			printf("\t 1. Inserir elemento\n");
			printf("\t 2. Retirar elemento \n");
			printf("\t 3. Tamanho da lista\n");
			printf("\t 4. Printar elemento\n");
			printf("\t 5. Sair\n");
			printf("-- Digite sua escolha: ");
			scanf("%d", &c);
		} while(c<=0 || c>5);
		printf("\n");
		
return c;
}

void emp(desc_pilha * descritor){
struct elemento *aux;
int tf;	

	aux = malloc(sizeof(struct elemento));
	
	printf("1. Inserir \n");
	printf("------------------------\n");
	printf("  Elemento: ");
	scanf("%d", &aux->chave);
	
	
	tf = push(aux, descritor);
	
 // ----------------	
	
	if (tf == 1) {
		printf("OK! Elemento inserido.\n");
		printf("\n");
	} else {
		printf("Erro! Elemento nao inserido.\n");
		printf("\n");
	}
}

void dezemp(desc_pilha *descritor){
	int tf;
	
	if(descritor->head == NULL){
		tf = 0;
	} else {  
		tf = pop(descritor->tail->ptr, descritor);
	}
	
	if (!tf) {
		printf("Lista vazia.");
		printf("\n");	
	} else {
		printf("Elemento excluido.");
		printf("\n");
	}
}

void tamanho(desc_pilha * descritor){
	
	if (length(descritor) != 0){
		printf("Tamanho da Pilha: %d\n", length(descritor));
		printf("\n");
	} else {
		printf("Lista vazia.\n");
		printf("\n");
	}
}

int printar(desc_pilha * descritor){
struct nodo *temp;
int i, posicao;

	printf("4. Printar\n");
	printf("------------------------\n");
	printf("\n");
	
	if (descritor->head == NULL){
		printf("Lista vazia.\n");
		return 1;
	}
	
	printf("  Lista de Elementos\n");
	printf("  -----\n");
	
	temp = descritor->tail;
	i = descritor->tamanho;
	printf(" (POS) - [ELE] \n");
		while(temp != NULL) {
			printf("  (%d)  - [%d]\n", i, temp->ptr->chave);
			temp = temp->prev;	
			i--;
		}
		printf("\n");
		
		
	//  get improvisado	
	// ----------------------	
		
		printf("Posicao: ");
		scanf("%d", &posicao);
		
		temp = descritor->tail;
		i = descritor->tamanho;
		while(temp != NULL){
			if (i == posicao){
				print(temp);
				return 1;
			} else { 
				temp = temp->prev;
				i--;
			}
		}
	
		printf("\n");
		printf("Elemento nao existe.\n");
		printf("\n");
	
	return 1;
}
