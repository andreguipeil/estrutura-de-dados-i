#ifndef PILHA_H
#define PILHA_H

struct elemento {
	int chave;
};

struct nodo {
	struct elemento * ptr;
	struct nodo *prev;
};

typedef struct { 
	
	struct nodo *head, *tail;
	unsigned int tamanho;
	
} desc_pilha;

// mainpilha.c

int main();
int menu(desc_pilha * descritor);
int printar(desc_pilha * descritor);
void emp(desc_pilha * descritor);
void dezemp(desc_pilha * descritor);
void tamanho(desc_pilha * descritor);


// ----------------------------

// pilha.c

// aloca, inicializa e retorna um ponteiro para um descritor de pilha (ou NULL);
desc_pilha * init();
// insere um novo elemento na pilha, retornando 0 em caso de erro, 1 se OK.
int push(struct elemento * x, desc_pilha * descritor);
//retira um elemento da pilha, colocando-o no elemento x, retornando 0 em caso de erro e 1 se OK;
int pop(struct elemento * x, desc_pilha * descritor);
// retorna o comprimento da pilha;
unsigned int length(desc_pilha * descritor);
// imprime conteúdo do nodo apontado por ptr e retorna 0 em caso de erro ou 1.
int print(struct nodo * ptr);

#endif
