#include <stdio.h>
#include <stdlib.h>
#include "lde.h"


// Andre Guimaraes Peil - ED1

// ------------------------------------------------------------------------------
// Funcao main
// ------------------------------------------------------------------------------

int main() {

desc_lista *descritor;
int escolha;
descritor = init();
	
	
	for ( ; ; ){
		escolha = menu(descritor);
		switch(escolha){
			
			case 1: inserir(descritor);
			break; 
			case 2: deletar(descritor);
			break; 
			case 3: loc(descritor);
			break;
			case 4: alterar(descritor);
			break;
			case 5: pegar(descritor);
			break;
			case 6: tamanho(descritor);  
			break;
			case 7:
					printf("7. Lista de Elementos");
					printf("\n");
					printf("------------------------------------------------------");
					printf("\n");
					listar(descritor); 
					break;
			case 8: exit(0);
			break;
		
		}
	}
}




// ------------------------------------------------------------------------------
// Funcao menu
// ------------------------------------------------------------------------------

int menu(desc_lista *descritor) {
	int c=0;
		
		printf("PRESSIONE ENTER PARA CONTINUAR...");
		getchar();
		getchar();
		system("clear");
		
		printf("\n");
		printf("--- ");
		printf("\n");
		printf("Lista Duplamente Encadeada");
		printf("\n");
		
		do {
			printf("-- MENU:\n");
			printf("\t 1. Inserir elemento\n");
			printf("\t 2. Deletar elemento \n");
			printf("\t 3. Localizar NODO\n");
			printf("\t 4. Alterar elemento\n");
			printf("\t 5. Pegar NODO\n");
			printf("\t 6. Tamanho da lista\n");
			printf("\t 7. Mostra lista\n");
			printf("\t 8. Sair\n");
			printf("-- Digite sua escolha: ");
			scanf("%d", &c);
		} while(c<=0 || c>8);
		printf("\n");
		
return c;
}




// ------------------------------------------------------------------------------
// Funcao inserir
// ------------------------------------------------------------------------------

void inserir(desc_lista	*descritor){
int elemento, posicao, tf;
struct nodo *temp;

		printf("\n");
		printf("1. Inserir Elemento");
		printf("\n");
		printf("-----------------------------------------");
		printf("\n");
		printf(" -- Insira um elemento: ");
		scanf("%d", &elemento);
		printf(" -- Insira a posicao: ");
		scanf("%d", &posicao);
		printf("\n");
		
	// verifica se as posicao e valida
		if ((posicao > descritor->tamanho) || (posicao < 0)){
			tf =  0;
			
		} else {
				temp = get(posicao-1,descritor);
				tf = insert(temp, elemento, descritor);
		}
		
// ------------------------
		
		if (tf == 1){
			printf("\n");
			printf(" Ok!! nodo cadastrado!");
			printf("\n");
		} else {
			printf("\n");
			printf(" Erro viajasse!!");
			printf("\n");	
		}
		



} 
 



// ------------------------------------------------------------------------------
// Funcao deletar
// ------------------------------------------------------------------------------

void deletar(desc_lista *descritor){
int ele, tf;
struct nodo *ptr, *aux;


// imprime a lista de elementos
//-------------------------------------	
	printf("\n");
	printf("2. Deletar Elemento");
	printf("\n");
	printf("-----------------------------------------");
	printf("\n");	
	printf("  Elementos cadastrados");
	printf("\n");
	printf("-----------------------------------------");
	printf("\n");
	ptr = descritor->head;
		while(ptr != NULL) {
			printf(" [%d] -", ptr->elemento);
			ptr = ptr->next;	
		}
	printf("\n");

// --------------------	

	aux = descritor->head;
	// se tiver somente 1 elemento na lista
	if (descritor->tamanho == 1) {
		delete(aux, descritor);
		printf("PARABENS!! você excluiu o unico elemento da lista.");
		printf("\n");
	} else { // caso contrario {
		printf("\n");	
		printf(" -- Diga elemento/nodo que deseja excluir: ");
		scanf("\n%d", &ele);
		aux = locate(ele, aux);
		
		if (aux == NULL){
			printf("\n");
			printf(" Elemento não encontrado.");
			printf("\n");
		} else {
			tf = delete(aux, descritor); 
		}
	
	
// ------------------------------------------------	
		
		if (tf == 1){
			printf(" Ok! posicao excluida!!");
			printf("\n");
			printf("\n");
			
			if (aux == NULL){
				printf("\n");
				printf("Lista vazia");
				
			}
// se tem elemento na lista apresenta a lista
// --------------------------------------------------
			
			if (descritor->tamanho != 0){
				printf(" Elementos cadastrados");
				printf("\n");
				printf("-----------------------------------------");
				printf("\n");
				ptr = descritor->head;
				
				while(ptr != NULL) {
					printf(" [%d] -", ptr->elemento);
					ptr = ptr->next;	
				}
				printf("\n");
			}
		}
	}
}

// --------------------------------------------------------------------
// funcao loc
// --------------------------------------------------------------------
void loc(desc_lista *descritor){
int ele, posicao, tf; 	
struct nodo *p, *de;

	
	
	printf("3. Localizar NODO");
	printf("\n");
	printf("-----------------------------------------");
	printf("\n");
	printf(" -- Elemento: ");
	scanf("%d", &ele);
	printf(" -- A partir da posicao: ");
	scanf("%d", &posicao);
	
	// retorna o nodo a partir da posicao que o usuario digitou
	de = get(posicao, descritor);
	p = locate(ele, de);	

// ------------------------------
	
	if (p == NULL){
		printf("\n");
		printf(" Elemento nao existe.");
		printf("\n");
		printf("\n");
	} else {
		printf("\n");

		printf("Nodo encontrado após a posicao: %d", posicao);
		tf = print(p);	
		if (tf != 1)	
			printf("  Elemento nulo.");
	}
}	



 
// ------------------------------------------------------------------------------
// Funcao alterar
// ------------------------------------------------------------------------------

void alterar(desc_lista *descritor){
int elemento, x, tf;
struct nodo *ptr, *aux;

// apresenta elementos cadastrados
// ---------------------------------------	
	printf("4. Alterar Elemento");
	printf("\n");
	printf("-----------------------------------------");
	printf("\n");
	printf("\n");
	printf("  Elementos cadastrados");
	printf("\n");
	printf("-----------------------------------------");
	printf("\n");
	ptr = descritor->head;
		while(ptr != NULL) {
			printf(" [%d] -", ptr->elemento);
			ptr = ptr->next;	
		}
	printf("\n");

// ------------	
	
		printf("\n");
		printf(" -- Elemento que deseja substituir: ");
		scanf("\n %d", &elemento);
		aux = descritor->head;
		
		
	// 	localiza se o elemento existe
		if (locate(elemento, aux) == NULL){
			printf("\n");
			printf(" Elemento não encontrado.");
			printf("\n");
			tf = 0;
		} else { // caso contrario
			
			ptr = locate(elemento, aux);
	//  inclui novo elemento para substituir existente
				printf(" -- Novo elemento: ");
				scanf(" %d", &x);
				printf("\n");
				tf = set(ptr, x, descritor);
		}
		
		
// ------------------------------------------------
		
		if (tf == 1){
			printf(" Ok! TROCA TROCADA!!!");
			printf("\n");
			printf("\n");
			printf("  Elementos cadastrados");
			printf("\n");
			printf("----------------------------------");
			printf("\n");
			
			ptr = descritor->head;
			while(ptr != NULL) {
				printf(" [%d] -", ptr->elemento);
				printf("\n");
				ptr = ptr->next;	
			}
			printf("\n");	 
		}

}



// ------------------------------------------------------------------------------
// Funcao pegar
// arrumar ELSE NO FINAL
// ------------------------------------------------------------------------------

void pegar(desc_lista * descritor){
unsigned int posicao;
int i = 0;
struct nodo * de, * ptr;

	printf("5. Pegar NODO");
	printf("\n");
	printf("-----------------------------------------");
	printf("\n");	
	printf("\n");
	printf("  Escolha a [Posicao] ");
	printf("\n");
	printf("-----------------------------------------");
	printf("\n");
	ptr = descritor->head;
		while(ptr != NULL) {
			printf("[%d] - ", i);
			ptr = ptr->next;
			i++;	
		}
	printf("\n");
	
// -----------

    // usuario insere posicao para apresentar o nodo
	printf(" -- Posicao: ");
	scanf("%d", &posicao);
	
// --------------------------------------------- 	
	
	// verifica se a posicao eh corespondente
	if ((posicao < descritor->tamanho)){
		de = get(posicao, descritor);
		printf(" NODO: ");
		print(de);
	} else { // caso contrario
		printf(" Elemento nao encontrado.");
		printf("\n");
	}
}




// ------------------------------------------------------------------------------
// Funcao tamanho
// ------------------------------------------------------------------------------

void tamanho(desc_lista * descritor) {	

	// verifica se a lista esta vazia
	if (length(descritor) == 0 ){
		printf("Lista Vazia");
		printf("\n");
	} else {	 // caso contrario 
	printf("6. Tamanho da Lista");
	printf("\n");
	printf("-----------------------------------------");
	printf("\n");
	printf("\n");
	printf(" Tamanho: %d ", length(descritor));
	printf("\n");
	printf("--------\n");
	}
}




// ------------------------------------------------------------------------------
// Funcao listar
// ------------------------------------------------------------------------------

void listar(desc_lista *descritor){
	struct nodo * ptr;
	ptr = descritor->head;
		
		
	// verifica se a lista esta vazia 	
	if (length(descritor) == 0 ){
		printf("Lista Vazia");
		printf("\n");
	} else { // caso contrario
		
		printf("\n");	
		printf("\n");
		while(ptr != NULL) {
			printf("{ %p } - [%d] - { %p } \n", ptr->prev, ptr->elemento, ptr->next);
			ptr = ptr->next;	
		}
			printf("\n");
			printf("------------------------------------------------------\n");
			printf("\n");
			printf("\n");
	}
}
