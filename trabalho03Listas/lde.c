#include <stdio.h>
#include <stdlib.h>
#include "lde.h"

// Andre Guimaraes Peil - ED1


// funcao INIT 
// aloca, inicializa e retorna um ponteiro para um descritor de lista;
//----------------------------------------------------------------------
desc_lista *init(){
	
	desc_lista *list;
		
		if (!(list = malloc(sizeof(desc_lista)))){
			printf("\n");
			printf("erro.");
			printf("\n");
			return 0;	
		}
		
		list->head = NULL;
		list->tail = NULL;
		list->tamanho = 0;
		
return list;
}

// funcao INSERT
// insere um novo elemento após anterior (ou no início da lista, se NULL) e retorna 0 em caso de erro, 1 se OK.
//----------------------------------------------------------------------
int insert(struct nodo *anterior, int chave, desc_lista *descritor){
	struct nodo *de;
		
		de = malloc(sizeof(struct nodo));	
		de->elemento = chave;
		
		// CASO 1 anterior é NULL, primeiro elemento 	
		if (anterior == NULL){ 
			de->prev = NULL;
			de->next = descritor->head;
			descritor->head = de;
			
			// CASO 2 lista vazia
			if (descritor->tail == NULL){
				descritor->tail = de;
				descritor->tamanho++;
			return 1; 
			} else { // CASO 3 lista tinha elemento
				de->next->prev = de;
				descritor->tamanho++;
			
			return 1;
			}	
		} else { //CASO 4 insere no meio ou no fim
			de->prev = anterior; // é o next do elemento anterior 
			de->next = anterior->next;
			anterior->next = de;
			if (de->next == NULL) { // CASO 5 insere no fim
				descritor->tail = de;
				descritor->tamanho++;
			
				return 1;
			 } else { // CASO 6 insere no meio
				de->next->prev = de;
				descritor->tamanho++;
			
				return 1;
			}
		}
		
return 0;	
}



// Funcao GET
// recebe valores da funcao pegar() e calcula
// -------------------------------------------------------------------------------
struct nodo * get(unsigned int posicao, desc_lista * descritor){
	struct nodo * p;		
	int i;
	
	// zerando os contadores 	
		p = descritor->head;
		i = 0;
		
		if ((posicao > descritor->tamanho) && (posicao < 0)){
			return NULL;
		}
		
		while(i < descritor->tamanho){
			if (i == posicao){
				return p;
			} else { 
				i++;
				p = p->next;
			}
		}
return NULL;	
}



// funcao SET
// recebe um NODO e substitui o valor dele
//----------------------------------------------------------------------
int set(struct nodo * ptr, int x, desc_lista * descritor){

	ptr->elemento = x;
		
return 1;
}

// funcao DELETE
// recebe um NODO e exclui ele
//----------------------------------------------------------------------
int delete(struct nodo * ptr, desc_lista * descritor){


	// se tem 1 elemento.
	if ((ptr == descritor->head) && (ptr == descritor->tail)){
		descritor->head = NULL;
		descritor->tail = NULL;
	} else {
		if (ptr == descritor->tail) {
			descritor->tail = ptr->prev;
			ptr->prev->next = NULL;
		} else {
			ptr->next->prev = ptr->prev;
			if (ptr == descritor->head)
				descritor->head = ptr->next;
			else
				ptr->prev->next = ptr->next;
		}
	}
	descritor->tamanho--;		
return 1;
}

// funcao LOCATE
// retorna ponteiro para elemento com chave procurando a partir da posição apontada por de;
//----------------------------------------------------------------------
struct nodo *locate(int chave, struct nodo *de){
	
		while(de != NULL){
			if (chave == de->elemento)
				return de;
			de = de->next;	
		}
return NULL;	
}


// funcao PRINT
// apresenta NODO
//----------------------------------------------------------------------
int print(struct nodo * ptr){
	
	printf("\n");
	printf("%p [%d] %p", ptr->prev, ptr->elemento, ptr->next);
	printf("\n");
	
return 1;
}


// funcao length
// retorna tamanho da lista
//----------------------------------------------------------------------
unsigned int length(desc_lista *descritor){
	unsigned int p;	
	
		p = descritor->tamanho;

return p;
}

