// Andre Guimaraes Peil - ED1

#ifndef LDE_H
#define LDE_H

struct nodo {
	int elemento;
	struct nodo *prev, *next;	
};

typedef struct {
	struct nodo *head, *tail;
	unsigned int tamanho;
} desc_lista;




// ldetest.c
int menu(desc_lista * descritor);
void inserir(desc_lista * descritor);
void alterar(desc_lista * descritor);
void deletar(desc_lista * descritor);
void loc(desc_lista * descritor);
void pegar(desc_lista * descritor);
void listar(desc_lista * descritor);
void tamanho(desc_lista *descritor);

// lde.c
desc_lista *init();
int insert(struct nodo * anterior, int chave, desc_lista * descritor);
struct nodo * get(unsigned int posicao, desc_lista * descritor);
int set(struct nodo * ptr, int x, desc_lista * descritor);
int delete(struct nodo * ptr, desc_lista * descritor);
struct nodo *locate(int chave, struct nodo *de);
unsigned int length(desc_lista * descritor);
int print(struct nodo * ptr);



#endif 
