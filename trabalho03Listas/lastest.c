#include <stdio.h>
#include <stdlib.h>
#include "las.h"

// Andre Guimaraes Peil - ED1

// ------------------------------------------------------------------------------
// Funcao main
// ------------------------------------------------------------------------------

int main() {

desc_lista *descritor;
int escolha;
	
	descritor = init();
	
	for ( ; ; ){
		escolha = menu(descritor);
		switch(escolha){
			
			case 1: incluir(descritor);
			break;
			case 2: pegar(descritor);
			break; 
			case 3: subst(descritor);
			break;
			case 4: excluir(descritor);
			break;
			case 5: loc(descritor);
			break;  
			case 6: lenght(descritor); 
			break;
			case 7: exit(0);
			break;
		
		}
	}
}




// ------------------------------------------------------------------------------
// Funcao menu
// ------------------------------------------------------------------------------

int menu(desc_lista *descritor) {
int c=0, i;

	printf("Lista Sequencial");
	printf("\n");
	for (i = 0; i < descritor->last; i++){
		printf("%d = [ %d ] ", i, descritor->vetor[i]);
	} 
	printf("\n");
	do {
		printf("-- MENU:\n");
		printf("\t 1. Inserir um elemento\n");
		printf("\t 2. Pegar um elemento\n");
		printf("\t 3. Substituir elemento \n");
		printf("\t 4. Deletar elemento\n");
		printf("\t 5. Pesquisar elemento\n");
		printf("\t 6. Tamanho da lista\n");
		printf("\t 7. Sair\n");
		printf("-- Digite sua escolha: ");
		scanf("%d", &c);
	} while(c<=0 || c>7);
	getchar();
	
return c;
}




// ------------------------------------------------------------------------------
// Funcao incluir
// --------------
// FUNCAO MODIFICADA
// ------------------------------------------------------------------------------

void incluir(desc_lista	*descritor){
int elemento, posicao, tf;
 
		printf("Insira um elemento: ");
		scanf("%d", &elemento);
		printf("\n");
		printf("Insira a posicao: ");
		scanf("%d", &posicao);
		printf("\n");
		
		
		tf = insert(posicao, elemento, descritor);

// ------------------------------------------------

		if(tf == 1) {
			printf("\n");
			printf("Ok! elemento Cadastrado!");
			printf("\n");
			printf("\n");
		} else {
			printf("\n");
			printf("Erro!!! VIAJASSE!");
			printf("\n");
			printf("\n");
		}
}




// ------------------------------------------------------------------------------
// Funcao pegar
// ------------------
// funcao recebe valores do usuario e chama a funcao GET
// ------------------------------------------------------------------------------

void pegar(desc_lista *descritor){
unsigned int posicao;
int  i;
int *p;	

	printf("\n");	
	printf("Posições Validas: ");
	for (i = 0; i < descritor->last; i++) {
		printf("%d = [ %d ] ", i, descritor->vetor[i]);
	}
	
	printf("\n");
	printf("Digite a posição em que deseja acessar: ");
	scanf("%d", &posicao);
	printf("\n");
	
	p = get(posicao, descritor);
	
	if (p != NULL){
		
		printf("\n");
		printf("Valor desta posicao: ");
		printf("%d", *p);
		printf("\n");
		printf("\n");	
		
	} else {
		printf("\n");
		printf("Valor nao existe.");
		printf("\n");
		printf("\n");
	
		
	}
}
	
 
 
 
// ------------------------------------------------------------------------------
// Funcao subst
// ------------------
// MODIFICADA
// ------------------------------------------------------------------------------

void subst(desc_lista *descritor){
int posicao, i, x, tf;

	printf("\n");
	printf("Posições Validas: ");
	for (i = 0; i < descritor->last; i++) {
		printf("%d = [ %d ] ", i, descritor->vetor[i]);
	} 
	
		printf("\n");
		printf("Digite a posição em que deseja substituir: ");
		scanf("%d", &posicao);
		printf("\n");
	
		printf("Valor que deseja substituir: ");
		scanf("%d", &x);
		printf("\n");
		tf = set(posicao, x, descritor);
		
// ------------------------------------------------
		
		if (tf = 1){
			printf("Ok! TROCA TROCADA!!!");
			printf("\n");
			for (i = 0; i < descritor->last; i++){
				printf("%d = [ %d ] ", i, descritor->vetor[i]);
			}	 
		} else
			printf("Erro!!! VIAJASSE!");
			printf("\n");
}




// ------------------------------------------------------------------------------
// Funcao excluir
// ------------------
// funcao recebe valores do usuario e chama a funcao DELETE
// ------------------------------------------------------------------------------

void excluir(desc_lista *descritor){
int posicao, i, tf;

	printf("\n");
	printf("Posições Validas: ");
	for (i = 0; i < descritor->last; i++){
		printf("%d = [ %d ] ", i, descritor->vetor[i]);
	}
	
	printf("\n");	
	printf("Diga a posicao que deseja excluir: ");
	scanf("%d", &posicao);
	tf = delete(posicao, descritor);
	printf("\n");
	
// ------------------------------------------------	
	
	if (tf == 1){
		printf("Ok! posicao excluida!!");
		printf("\n");
		for (i = 0; i < descritor->last; i++){
			printf("%d = [ %d ] ", i, descritor->vetor[i]);
		}
	} else
		printf("Erro VIAJASSE!!!");
		printf("\n");
}

void loc(desc_lista *descritor){
int elemento;	
int *p;	

	printf("\n");
	printf("Elemento: ");
	scanf("%d", &elemento);
	printf("\n");
	p = locate(elemento, descritor);
	
	if (p == NULL){
		printf("\n");
		printf("Valor nao existe.");
		printf("\n");
		printf("\n");
	} 
		
}
