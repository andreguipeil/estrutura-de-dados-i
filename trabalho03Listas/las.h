// Andre Guimaraes Peil - ED1

#ifndef LAS_H
#define LAS_H

#define TAM 10

typedef struct {
	int vetor[TAM];
	int last;
} desc_lista;

// lastest.c
int menu(desc_lista *descritor);
void incluir(desc_lista *descritor);
void pegar(desc_lista *descritor);
void subst(desc_lista *descritor);
void excluir(desc_lista *descritor);
void loc (desc_lista *descritor);

//las.c
desc_lista *init();
int insert(unsigned int posicao, int chave, desc_lista *descritor);
int *get(unsigned int posicao, desc_lista *descritor);
int delete(unsigned int posicao, desc_lista *descritor);
int *locate(int chave, desc_lista *descritor);
unsigned int lenght(desc_lista *descritor);

#endif 
