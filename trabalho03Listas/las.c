#include <stdio.h>
#include "las.h"
#include <stdlib.h>

// Andre Guimaraes Peil - ED1


// ------------------------------------------------------------------------------
// funcao init
// ------------------------------------------------------------------------------
  desc_lista *init(){
	desc_lista *p;
	p = malloc(sizeof(desc_lista));
	
	p->last = 0;
	
	return p; 
}

// -------------------------------------------------------------------------------
// Funcao INSERT
// MODIFICADA
// -------------------------------------------------------------------------------

int insert(unsigned int posicao, int chave, desc_lista *descritor){
int i, ret;
	
	
	if ((posicao > descritor->last) || (posicao < 0))
		return 0;	
	if (descritor->last >= TAM)
		return 0;
	
	else {
		
		if (posicao == descritor->last){
			descritor->vetor[posicao]= chave;
			descritor->last++;
		} 
		else
		{
			for (i = descritor->last-1; i >= posicao; i--){
				descritor->vetor[i+1] = descritor->vetor[i];
			}	
			descritor->vetor[posicao] = chave;
			descritor->last++;
		}
		ret = 1;	
	}
	

return 	ret;
}




// -------------------------------------------------------------------------------
// Funcao GET
// recebe valores da funcao pegar() e calcula
// -------------------------------------------------------------------------------
	
int *get(unsigned int posicao, desc_lista *descritor){
int	*p;		
	
	p = NULL;
			
	p = &descritor->vetor[posicao];	
			
return p;
}




// -------------------------------------------------------------------------------
// Funcao SET
// MODIFICADA
// -------------------------------------------------------------------------------

int set(unsigned int posicao, int x, desc_lista *descritor){
	int ret;
	
	if ((posicao < 0) && (descritor->last <= posicao))
		ret = 0;
	else {
		descritor->vetor[posicao] = x;
		ret = 1;
	}
	
return ret;		
}




// -------------------------------------------------------------------------------
// Funcao DEL
// MODIFICADA
// -------------------------------------------------------------------------------

int delete(unsigned int posicao, desc_lista *descritor){
	int q, ret;
		
	if (descritor->last < posicao){
		ret = 0;	
	}
	else {
		descritor->last = descritor->last-1;
		for (q = posicao; q < descritor->last; q++)
			descritor->vetor[q] = descritor->vetor[q+1];
		ret = 1;
	}
return ret;	
}

int *locate(int chave, desc_lista *descritor){
 int *p;	
 int i;	
	for (i = 0; i <= descritor->last; i++){
		if (descritor->vetor[i] == chave){
			p = &descritor->vetor[i];
			printf("\n");
			printf("Posicao - Ponteiro - Elemento: [%d] - [%p] - [%d]", i, p, chave);
			printf("\n---\n");
			return p;	
		}
	}
	return NULL;
}




// -------------------------------------------------------------------------------
// Funcao LENGHT
// MODIFICADA
// -------------------------------------------------------------------------------

unsigned int lenght(desc_lista *descritor){
	unsigned int aux;
	
	aux = descritor->last;
	printf("\n");
	printf("Tamanho da Lista: %d ", aux);
	printf("\n");
	printf("--------\n");	
	
return aux;	
}
