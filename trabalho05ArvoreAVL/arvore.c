/* 
 Trabalho Final - Estrutura de Dados
	
	Tema: Arvore AVL
	Feito por: Andre Guimaraes Peil - 10206132

*/

#include <stdio.h>
#include <stdlib.h>
#include "arvorelib.h"

// ------------------------------------------------------------------------------
// Funcao init
// ------------------------------------------------------------------------------
tree * init()
{
  tree * figueira;
	
	if (!(figueira = malloc(sizeof(tree)))){
		printf("\n");
		printf("Erro no malloc da arvore.");
		printf("\n");
		return NULL;	
	}

	figueira->raiz = NULL;
	
return figueira;
}

// ------------------------------------------------------------------------------
// Funcao insert
// ------------------------------------------------------------------------------
nodo * insert (nodo * raiz, element * x) 
{
		
	if (raiz != NULL){ 				// se a raiz for diferente de null entra na recursao	
		
		// se for igual
		if (x->valor == raiz->chave->valor){
			return raiz;
		}	
			
		if (x->valor < raiz->chave->valor){		
			raiz->esq = insert(raiz->esq, x);	 
		} else {
			if (x->valor > raiz->chave->valor){		
				raiz->dir = insert(raiz->dir, x);
			}	
		}
		raiz = balanceamento(raiz);	
	} else {						// senao aloca o nodo e o valor
		
		if(!(raiz = malloc(sizeof(nodo)))){
			printf("\n");
			printf("Erro no malloc do nodo.");
			printf("\n");
			return NULL;
		}
		
		raiz->chave = x;
		raiz->dir = NULL;
		raiz->esq = NULL;
	}
		 
	return raiz;
}

// ------------------------------------------------------------------------------
// Funcao fator de balanceamento
// ------------------------------------------------------------------------------
int fat_bal(nodo * raiz)
{
	
	if (raiz == NULL){
		return 0;
	} else {	
		return (altura(raiz->dir) - altura(raiz->esq));
	}
	
}

// ------------------------------------------------------------------------------
// Funcao balanceamento
// ------------------------------------------------------------------------------
nodo * balanceamento(nodo * raiz)
{
	int fb = fat_bal(raiz);
	

	
	if ((fb <= -2) || (fb >=2)){
		printf("-- Acao do Balanceamento: \n");
		if (fb > 0){
			fb = fat_bal(raiz->dir);
			if (fb >= 0){
				
				printf(" rot simples a esq \n");
				raiz = rot_esq(raiz);
			} else {
				printf(" rot dupla a esq.\n");
				raiz = rd_esq(raiz);				
			}	 
		} else {
			fb = fat_bal(raiz->esq);
			if (fb <= 0){
				printf(" rot simples a dir\n");
				raiz = rot_dir(raiz);	
			} else {
				printf(" rot dupla a dir\n");
				raiz = rd_dir(raiz);
				
			}
		}
	}	
	return raiz;
}	

// ------------------------------------------------------------------------------
// Funcao find
// ------------------------------------------------------------------------------
nodo * find (nodo * raiz, element * x)
{
		
		if (raiz == NULL){
			return NULL;
		}
		
		if (raiz->chave->valor > x->valor){
		 raiz =	find(raiz->esq, x);
			
		} else {
			if (raiz->chave->valor < x->valor){
			 raiz =	find(raiz->dir, x);
			}
		}
		
 return raiz;
}

// ------------------------------------------------------------------------------
// Funcao exclude
// ------------------------------------------------------------------------------
nodo * exclude (nodo * raiz, element * x) 
{

	nodo * temp, * filho;
	
	// se a arvore estiver vazia
		if (raiz == NULL){
			printf("Erro nodo nao encontrado");
			return NULL;
		} else if (x->valor < raiz->chave->valor){		// esquerda
				raiz->esq = exclude(raiz->esq, x);
				raiz = balanceamento(raiz);
				} else if ( x->valor > raiz->chave->valor){		// direita
							raiz->dir = exclude(raiz->dir, x);	
							raiz = balanceamento(raiz);
						} else if (raiz->esq && raiz->dir) {	// se tiver 2 filhos
									temp = find_min(raiz->dir);	
									raiz->chave->valor = temp->chave->valor;
									raiz->dir = exclude(raiz->dir, raiz->chave);
								} else { 					// filho unico
									temp = raiz;
									if (raiz->esq == NULL)  // so tem filho a direita
										filho = raiz->dir;							
									if (raiz->dir == NULL){  // so tem filho a esquerda
										filho = raiz->esq;
									}	
									free(temp);
									return filho;
								}			
	return raiz;
}	

// ------------------------------------------------------------------------------
// Funcao find_min
// ------------------------------------------------------------------------------
nodo * find_min(nodo * raiz)
{
	
	if (raiz->esq != NULL){
		raiz = find_min(raiz->esq);
	}
	return raiz;

}

// ------------------------------------------------------------------------------
// Funcao altura
// ------------------------------------------------------------------------------
int altura (nodo * raiz)
{
int esq, dir;

	if (raiz ==  NULL){
		return -1;
	} else { 
	
		esq = altura(raiz->esq);
		dir = altura(raiz->dir);

	if ( esq > dir )
		return esq + 1;
	else
		return dir + 1;
	}
}

// ------------------------------------------------------------------------------
// Funcao rotacao a direita
// ------------------------------------------------------------------------------
nodo * rot_dir(nodo * x)
{
	nodo * y;
	
	if (x != NULL){
		y = x->esq;
		x->esq = y->dir;
		y->dir = x;
	}
	
	return y;
	
}

// ------------------------------------------------------------------------------
// Funcao rotacao a esquerda
// ------------------------------------------------------------------------------
nodo * rot_esq(nodo * x)
{
	nodo * y;
	
	if (x != NULL){
		y = x->dir;
		x->dir = y->esq;
		y->esq = x;
	}

	return y;

}

// ------------------------------------------------------------------------------
// Funcao rotacao dupla a esquerda
// ------------------------------------------------------------------------------
nodo * rd_esq(nodo * x)
{

	x->dir = rot_dir(x->dir);
	x = rot_esq(x);
	
	return x;
}

// ------------------------------------------------------------------------------
// Funcao rotacao dupla a direita
// ------------------------------------------------------------------------------
nodo * rd_dir(nodo * x)
{
	x->esq = rot_esq(x->esq);	
	x = rot_dir(x);
	
	return x;
}

