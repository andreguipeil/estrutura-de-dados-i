/* 
 Trabalho Final - Estrutura de Dados
	
	Tema: Arvore AVL
	Feito por: Andre Guimaraes Peil - 10206132

*/

#include <stdio.h>
#include <stdlib.h>
#include "arvorelib.h"


// ------------------------------------------------------------------------------
// Funcao main
// ------------------------------------------------------------------------------
int main() {

tree * figueira;
int escolha;
figueira = init();
	
	for ( ; ; ){
		escolha = menu();
		switch(escolha){
			
			case 1:inserir(figueira);
			break; 
			case 2: excluir(figueira);
			break; 
			case 3: pesquisar(figueira);
			break;
			case 4: h(figueira);	
			break;
			case 5: { 
					printf("\n");
					printf("5. Em ordem\n");
					printf("------------------------\n");
						em_ordem(figueira, figueira->raiz);
						printf("\n");
			}
			break;
			case 6: {
					printf("\n");
					printf("6. Pre ordem\n");
					printf("------------------------\n");
						pre_ordem(figueira, figueira->raiz);
						printf("\n");
			}
			break;
			case 7: {
					printf("\n");
					printf("7. Pos ordem\n");
					printf("------------------------\n");
						pos_ordem(figueira, figueira->raiz);
						printf("\n");
				}
			break;
			case 8: exit(0);
			break;
		}
	}
}

// ------------------------------------------------------------------------------
// Funcao menu
// ------------------------------------------------------------------------------
int menu() 
{
	int c=0;
		
		printf("PRESSIONE ENTER PARA CONTINUAR...");
		getchar();
		getchar();
		system("clear");
		
		printf("\n");
		printf("--- ");
		printf("\n");
		printf("JESUS EH O JARDINEIRO E ASA AVRES SOMOS NOSES");
		printf("\n");
		
		do {
			printf("-- MENU:\n");
			printf("\t 1. Incluir   		\n");
			printf("\t 2. Excluir   		\n");
			printf("\t 3. Pesquisar 		\n");
			printf("\t 4. Altura da arvore 	\n");
			printf("\t -- 					\n");
			printf("-- LISTAR ARVORE:		\n");
			printf("\t 5. Em ordem  		\n");
			printf("\t 6. Pre ordem  		\n");
			printf("\t 7. Pos ordem  		\n");
			printf("\t-- 					\n");
			printf("\t 8. Sair      		\n");
			printf("--------------------------------\n");
			printf("-- Digite sua escolha: ");
			scanf("%d", &c);
		} while(c<=0 || c>9);
		printf("\n");
		
return c;
}

// ------------------------------------------------------------------------------
//  MENU - 1
// ------------------------------------------------------------------------------

void inserir(tree * figueira){
 element * temp;

	
	if (!(temp = malloc(sizeof(element)))){
		printf("\n");
		printf("Erro no malloc do elemento.");
		printf("\n");
		exit(1);
	}
	
	
	printf("1. Incluir \n");
	printf("------------------------\n");
	printf("  Elemento: ");
	scanf("%d", &(temp->valor));
	printf("\n");
	
	figueira->raiz = insert(figueira->raiz, temp);

 // ----------------	
	
	if (figueira->raiz != NULL) {
		printf("OK! Elemento inserido.\n");
		printf("\n");
	
	} else {
		printf("Erro! Elemento nao inserido.\n");
		printf("\n");
	}
	
}

// ------------------------------------------------------------------------------
//  MENU - 2
// ------------------------------------------------------------------------------

void excluir(tree * figueira){
	element * temp;
    nodo * aux;
	
		if (!(temp = malloc(sizeof(element)))){
		printf("\n");
		printf("Erro no malloc do elemento.");
		printf("\n");
		exit(1);
	}
	
	printf("Elementos inseridos: ");
	aux = figueira->raiz;
	em_ordem(figueira, aux);
	
	printf("\n");
	printf("2. Excluir\n");
	printf("------------------------\n");
	printf("Elemento: ");
	scanf("%d", &(temp->valor));
	printf("\n");
	
	exclude(figueira->raiz, temp);
	
 // ----------------	
	
	if (aux == NULL) {
		printf("Arvore vazia\n");
		printf("\n");
		
	} else {
		printf("Ok! Elemento excluido.\n");
		printf("\n");
	}
	 
}

// ------------------------------------------------------------------------------
//  MENU - 3
// ------------------------------------------------------------------------------

void pesquisar (tree * figueira)
{
	element * temp;
	nodo * aux;
	
	
	temp = malloc(sizeof(element));
	printf("\n");
	printf("3. Pesquisar\n");
	printf("------------------------\n");
	printf("Elemento: ");
	scanf("%d", &(temp->valor));
	printf("\n");
	
	aux = find(figueira->raiz, temp);
	
	if (aux == NULL) {
		printf("\n");
		printf("Elemento nao pertence a arvore.");
		printf("\n");	
		
	} else {
	
		printf("\n");
		printf("Nodo Encontrado, ");
		display(aux);
		printf("\n");	
	}
	
	
}

// ------------------------------------------------------------------------------
//  MENU - 4
// ------------------------------------------------------------------------------

void h(tree * figueira)
{
	printf("\n");
	printf("4. Altura da arvore\n");
	printf("------------------------\n");
	printf("Altura: %d", altura(figueira->raiz));
	printf("\n");
	
}

// ------------------------------------------------------------------------------
//  MENU - 5
// ------------------------------------------------------------------------------

void em_ordem(tree * figueira, nodo * temp){



	if (temp != NULL){
		em_ordem(figueira, temp->esq);
		display(temp);
		em_ordem(figueira, temp->dir);
	}
}

// ------------------------------------------------------------------------------
//  MENU - 6
// ------------------------------------------------------------------------------

void pre_ordem(tree * figueira, nodo * temp){

	if (temp != NULL){
		display(temp);
		pre_ordem(figueira, temp->esq);
		pre_ordem(figueira, temp->dir);
	}
}

// ------------------------------------------------------------------------------
//  MENU - 7
// ------------------------------------------------------------------------------

void pos_ordem(tree * figueira, nodo * temp){



	if (temp != NULL){
		pos_ordem(figueira, temp->esq);
		pos_ordem(figueira, temp->dir);
		display(temp);
	}
}

void display (nodo * temp)
{
	
	printf( "%d ", temp->chave->valor);	
	
}

