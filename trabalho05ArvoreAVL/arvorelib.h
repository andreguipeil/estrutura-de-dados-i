/* 
 Trabalho Final - Estrutura de Dados
	
	Tema: Arvore AVL
	Feito por: Andre Guimaraes Peil - 10206132

*/

#ifndef ARVORELIB_H
#define ARVORELIB_H

typedef struct {
	int valor;
} element;

	struct no {
		element * chave;
		struct no * esq, * dir;
		int bal;
	};
	// transformando a struct no em nodo posso chamar somente nodo agora
	typedef struct no nodo;
	
		typedef struct {
			nodo * raiz;
			unsigned int num_nodos;
		} tree;

// main.c
	int menu();
	void inserir(tree * figueira);
	void excluir(tree * figueira);
	void pesquisar(tree * figueira);
	void pre_ordem(tree * figueira, nodo * aux);
	void em_ordem(tree * figueira, nodo * aux);
	void pos_ordem(tree * figueira, nodo * aux);
	void display (nodo * temp);
	void h(tree * figueira);

// arvore.c
	tree * init();
	nodo * insert (nodo * raiz, element * x);
	nodo * exclude(nodo * raiz, element * chave);
	nodo * find(nodo * raiz, element * x); 
	nodo * find_min(nodo * raiz);
	nodo * rot_esq(nodo * x);
	nodo * rot_dir(nodo * x);
	nodo * rd_esq(nodo * x);
	nodo * rd_dir(nodo * x);
	nodo * balanceamento(nodo * raiz);
	int fat_bal(nodo * raiz);
	int altura(nodo * raiz);

#endif
