/* 
 * File:   ordena.h
 * Author: agpeil
 *
 */

#ifndef ORDENA_H
#define	ORDENA_H

int bubbleSort(int *vetor, int tamanho);
int *mergeSort(int *vetor, int tamanho);
int *merge(int *vetor, int fim);
#endif
