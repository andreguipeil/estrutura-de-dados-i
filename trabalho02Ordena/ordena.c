#include <stdio.h>
#include <stdlib.h>
#include "ordena.h"


/*
 BubbleSort
--------------------------------------------------------------------------------
 */
int bubbleSort(int *vetor,int tamanho) {
 int i, flag, aux, tam = tamanho, k;
 int compBubble = 0;

    // faça
    do
    {
      // enquanto i for menor que n-1, incrementa
	flag = 0;
	    
        for (i = 0; i < tam-1;i++) {
			compBubble++; // total de comparações
            if (vetor[i] > vetor[i+1]) {
                aux = vetor[i];
				vetor[i] = vetor[i+1];
                vetor[i+1] = aux;
                flag = 1;
                
            }
        }
    // enquanto flag for = a 1
    // OBS: flag será 1 quando houver mudança de posição
    } while(flag == 1);

return compBubble;
}


	 
int *merge(int *vetor, int fim) {
  int meio;
  int i = 0, j, k = 0, compMerge = 0;
  int *temp;		
	meio = fim / 2;
	j = meio;
 	
	// aloca espaço para o vetor
	temp = (int*) malloc(fim * sizeof(int));
	
  while ((i < meio) && (j < fim)) {
	compMerge++;
		if (vetor[i] < vetor[j]) {
			temp[k] = vetor[i];
			i++;
		}
		else {
			temp[k] = vetor[j];
			j++;
		}
    k++;
  }
   
  if (i == meio) {
    while (j < fim) {
      temp[k] = vetor[j];
      j++;
      k++;
    }
  }
  else {
    while (i < meio) {
      temp[k] = vetor[i];
      i++;
      k++;
    }
  }
 // vetor original recebe o auxiliar
  for (i = 0; i < fim; i++) {
    vetor[i] = temp[i];
  }
  
	free(temp);
	return compMerge;
}
 
int *mergeSort(int *vetor, int fim) {
	int meio;
	
		if (fim > 1) {
			meio = fim / 2;
			mergeSort(vetor, meio);
			mergeSort(vetor + meio, fim - meio);
			merge(vetor, fim);
		}	
}



