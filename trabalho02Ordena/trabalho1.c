#include <stdio.h>
#include <stdlib.h>
#include "ordena.h"

/*
 main
--------------------------------------------------------------------------------
 */
int main(int argc, char *argv[])
{
    FILE *fp;
    int *vet, *vet2;
    int i = 0, compMerge, compBubble, j;

        // abre o arquivo
	fp = fopen (argv[1], "r");

	if ((vet = malloc(sizeof(int))) == NULL){
            printf ("Não foi possivel alocar a memória.\n");
            exit(1);
	}

	// enquanto %d for diferente de ; passa para o vetor
	while(fscanf(fp,"%d[^;]", &vet[i]) && feof(fp) != 1) {
            while(fgetc(fp) != '\n' && feof(fp) != 1){
            }
            i++;
            vet = realloc(vet, (i + 1) * sizeof(int));
	}
	fclose (fp);
	
	vet2 = malloc((i + 1) * sizeof(int));
	
	// passa as matriculas para o vet2
	for(j = 0; j < i; j++){
		vet2[j] = vet[j];
	}
	
	compBubble = bubbleSort(vet, i);
	compMerge = mergeSort(vet2, i);	
	
	
	
	
/*
 Escreve no arquivo bubble.txt, o vetor ordenado.
--------------------------------------------------------------------------------
*/
FILE *fb;
int k;
    if((fb = fopen("bubble.txt", "w")) == NULL) {
        printf("nao foi possivel criar o arquivo. \n");
        exit(1);
    }
    
	fprintf(fb, "Comparações bubbleSort = %d \n\n", compBubble);
	fprintf(fb,"Matriculas Ordenadas \n");
	fprintf(fb,"----------------------------------------- \n");

    for (k = 0;k < i; k++){
        fprintf(fb, "%d;\n", vet[k]);
    }
    fclose(fb);
			
	
	
// Escreve no arquivo mergeSort.txt, o vetor ordenado.
//--------------------------------------------------------------------------------
	FILE *fm;
	int u;

    if((fm = fopen("mergeSort.txt", "w")) == NULL) {
        printf("nao foi possivel criar o arquivo. \n");
        exit(1);
    }

	fprintf(fm, "Comparações mergeSort = %d \n\n", compMerge);
	
	fprintf(fm,"Matriculas Ordenadas \n");
	fprintf(fm,"----------------------------------------- \n");
	
    for (u = 0;u < i; u++){
        fprintf(fm, "%d;\n", vet2[u]);
    }
    fclose(fm);

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	free(vet);
	free(vet2);
	
return (EXIT_SUCCESS);

}

